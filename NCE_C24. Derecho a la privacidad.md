###### Art. C.24 - Derecho a la privacidad

1. Inviolabilidad del domicilio
2. Secreto de las comunicaciones
3. Derecho a la propia imagen
4. LOPD

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20): Derecho a la libertad de expresión e información

[Anterior](NCE_C21.%20Derecho%20a%20la%libertad%20de%expresión.md) |

> **Concordancias:**
>
>
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20),
>
>
