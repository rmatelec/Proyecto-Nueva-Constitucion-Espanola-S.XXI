###### Art. B02 - Fuentes del ordenamiento jurídico ######

1. El ordenamiento jurídico español está compuesto por la Constitución, las _normas con fuerza de ley_, la _costumbre_ y los _principios generales del derecho_.
2. Las _leyes_ y _normas con fuerza de ley_ son textos escritos que regulan una determinada materia con caracter general, obligatorio y en abstracto, dentro del marco que establece la Constitución. Su elaboración y aprobación corresponde a los órganos e instituciones competentes, según el procedimiento previsto en el ordenamiento jurídico.
3. A falta de _normas con fuerza de ley_, o cuando éstas remitan a ella, se aplicará la _costumbre_ del ámbito social o territorial que corresponda; siempre y cuando tanto su existencia como su contenido sean conocidos, lleve en vigor el tiempo suficiente para ser generalmente considerada como jurídicamente vinculante y se refiera a cuestiones que no deban ser reguladas por _normas con fuerza de ley_.
4. Los principios generales del derecho se aplicarán en ausencia de _normas con fuerza de ley_ y _costumbres_ aplicables, sin perjuicio de su carácter informador del ordenamiento jurídico.
5. Los actos de aplicación, ejecución o desarrollo de las fuentes del ordenamiento jurídico que no tengan expresamente reconocido su carácter legislativo no constituyen fuentes del derecho. Quedan excluidas, en particular, la jurisprudencia de los órganos con _potestad jurisdiccional_ y la doctrina, tanto científica como judicial.

> **Concordancias:**
>
> **Equivalencias R78:** [Art. 1 C.Civil](https://boe.es/buscar/act.php?id=BOE-A-1889-4763#a1),
>
* **2017.11.22 @rmatelec**: Vigilar que el reglamento encaga bien con la definición de norma escrita

[Anterior](NCE_B01.%20Supremac%C3%ADa%20de%20la%20Constituci%C3%B3n.md) | [Siguiente](NCE_B03.%20Criterios%20de%20interpretaci%C3%B3n%20de%20las%20normas.md)
