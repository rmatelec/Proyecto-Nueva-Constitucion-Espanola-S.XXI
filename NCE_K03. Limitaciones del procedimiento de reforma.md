###### Art. K.03 Limitaciones al procedimiento de reforma ######

1. No se podrá iniciar ningún _procedimiento de reforma constitucional_ durante los _estados de suspensión de determinados aspectos del orden constitucional_.

2. Los _procedimiento de reforma constitucional_ que se estuvieran tramitando quedarán suspendidos hasta que concluyan los _estados de suspensión de determinados aspectos del orden constitucional_ que se hubieran declarado.

3. Los _estado de suspensión de determinados aspectos del orden constitucional_ no podrá limitar en forma alguna los trabajos previos para la presentación de una _propuesta de reforma constitucional_. En especial, la preparación del texto y la recogida de avales.

> **Concordancias:** _estados de suspensión de determinados aspectos del orden constitucional_
>
> **Equivalencia CE78:** [Art. 169](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a169): Iniciativa de Reforma Constitucional, [Art. 116](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a116): Estados de alarma, excepción y sitio (por remisión), 
>

[Anterior](NCE_K02.%20Iniciativa%20de%20reforma.md) | [Siguiente](NCE_K04.%20Propuesta%20de%20reforma.md)
