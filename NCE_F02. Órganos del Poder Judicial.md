###### Art. F02 - Órganos del Poder Judicial ######  

1. Son Órganos del Poder Judicial
    1. El Consejo de Justicia.
    2. Los Juzgados y Tribunales.
    3. La Fiscalía.
    4. La Policía Judicial.

2. La constitución, funcionamiento y gobierno de los Juzgados y Tribunales, así como el estatuto jurídico de los Jueces, Magistrados y Fiscales de carrera, que formarán un Cuerpo único, y del personal al servicio de la Administración de Justicia será regulado mediante Ley Orgánica.

> **Concordancias:**
>
> **Equivalencia CE78: 
>

[Anterior](NCE_F01.%20Jurisdicción.md) | [Siguiente](NCE_F10.%20Consejo%20de%20Justicia.md)