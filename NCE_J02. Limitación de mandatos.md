###### Art. J.02 - Limitación de mandatos

1. Ninguna persona podrá ejercer más de un mandato consecutivo un cargo de designación política o electiva.

> **Concordancias:** A.01 - Titulo, B.11 - Titulo....
>
> **Equivalencia CE78:** [Art. ??](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a??): ASUNTO, 
>
> * **rmatelec @2024.03.02** En su redacción actual, el diputado saliente por otro distrito se podría presentar.
