###### Art. C.23 - Libertad de información

1. Toda persona es libre de comunicar y recibir información veraz públicamente y sin un 

2. Quedan prohibidas: 
    1. Toda forma de censura.
    2. El secuestro de lo medios utilizados para el ejercicio de este derecho, su sujeción a examen previo, autorización o caución.
    3. Cualesquiera otras medidas que prevengan, restrinjan, impidan o coarten, de forma directa o indirecta, el pleno y libre ejercicio de esto derechos.

3. Se reconoce el derecho a réplica

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20): Derecho a la libertad de expresión e información

[Anterior](NCE_C21.%20Derecho%20a%20la%libertad%20de%expresión.md) |


La libertad de expresión es un principio que apoya la libertad de un individuo o un colectivo de articular sus opiniones e ideas sin temor a represalias, censura o sanción posterior.



###### Art. C.?? - Libertad de opinión y expresión
1. Toda persona tiene derecho a expresar públicamente y sin injerencias hechos, opiniones e ideas por cualquier medio, incluso cuando pudiera resultar de mal gusto, desagradable, ofensivo, dañino o aborrecible.
2. No corresponde a los poderes públicos determinar qué puede o no puede ser comunicado o expresado. Por tanto, no podrán prohibir, limitar o someter a control previo el ejercicio de este derecho


regular  bajo ningún concepto en función de su contenido o de las posibles reacciones al mismo.

restringirse mediante ningún tipo de censura previa

All persons shall be responsible for the expression of their opinions before a court.

> **Concordancias:**
>
>
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20),
>
>
