###### Art. K.02 Iniciativa de reforma.md ######

1. El _procedimiento de reforma constitucional_ se iniciará mediante la presentación, ante la _Asamblea Nacional_, de una _propuesta de reforma constitucional_ avalada por: 
    1. Una décima parte de la _Asamblea Nacional_ o
    2. Una centésima parte de las personas con derecho al sufragio activo para elegir a los representantes de la _Asamblea Nacional_.

2. El documento para avalar una _propuesta de reforma constitucional_ deberá contener o hacer referencia a la siguiente información, de modo que las personas que avalan puedan conocerla de forma inequívoca: 
    1. La identidad de sus promotores, 
    2. El texto de la _propuesta de reforma_, 
    3. La identidad de los portavoces encargados de la defensa de la _propuesta de reforma_ y sus suplentes, o el procedimiento para su desginación.
    4. Cualesquiera otras instrucciones, mandatos o normas de decisión que deben observar sus promotores y portavoces durante la tramitación, en caso de existir.

3. Los avales son válidos por un año desde su recogida, salvo que el avalista retire expresamente su apoyo a la _propuesta de reforma constitucional_ o pierda su capacidad para avalarla antes de que alcance el número de avales necesarios.

4. Cualquier avalista cuyo aval hubiera quedado invalidado puede volver a avalar la _propuesta de reforma constitucional_ si reúne los requisitos necesarios para hacerlo, descritos en este artículo.


Puede incluir 


> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 166](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a166): Iniciativa de Reforma Constitucional, [Art. 87](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a87): Iniciativa Legislativa (por remisión)
>

[Anterior](NCE_K01.%20Del%20contenido%20de%20la%20Constitución%20y%20su%20reforma.md) | [Siguiente](NCE_K03.%20Limitaciones%20del%20procedimiento%20de%20reforma.md)
