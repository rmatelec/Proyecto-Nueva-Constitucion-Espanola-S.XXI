###### Art. C05 - Capacidad de Obrar

1. La capacidad de obrar es la condición necesaria para la eficacia de los actos realizados.
2. Salvo que concurran circunstancias inhabilitantes, las personas adquieren plena capacidad de obrar en España el día en que cumplen los dieciocho años con carácter general.
3. Por ley, se podrá reducir la edad para adquirir la capacidad de obrar en aspectos particulares.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 12](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a12): Mayoría de edad, [Disp. Ad. 2](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#segunda): Mayoría de edad en el derecho privado foral
