###### Art. C06 - Igualdad ante la ley

1. Los españoles son iguales ante la ley, sin que pueda prevalecer discriminación o trato favorable por ninguna condición o circunstancia personal o social.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 14](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a14): Igualdad ante la ley
>
