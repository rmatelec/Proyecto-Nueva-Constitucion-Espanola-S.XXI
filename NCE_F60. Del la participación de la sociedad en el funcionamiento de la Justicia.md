#### CAPITULO SEXTO - Del la participación de la sociedad en el funcionamiento de la Justicia
##### Art. F60 - Acción Popular

1. Las personas y entidades que, sin ser directamente perjudicadas, deseen intervenir en defensa de la legalidad, de los derechos de terceros o del interés público[^F60t1] podrán personarse ante los órganos de la jurisdicción[^F60t2] para impulsar la acción de la justicia de forma plena, autónoma e independiente.

2. La ley regulará las medidas para evitar el uso espurio de la acción popular.

[//]: # (Fin)

[Anterior]() | [Siguiente]()

###### Notas

REFERENCIAS: [Art.125 CE78](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a125)

CONCORDANCIAS: (dentro del proyecto de Constitución)

PROBLEMAS POTENCIALES:

[^F60t1]: ¿Se puede bloquear la acción popular mediante una valoración previa de si la acción popular cumple los requisitos de defensa del interés público?

[^F60t2]: ¿Se puede promover la acción de la justicia en cualquier orden (¿penal, civil, administrativo?)