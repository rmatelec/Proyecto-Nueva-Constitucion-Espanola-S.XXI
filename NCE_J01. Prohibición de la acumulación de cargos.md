### TITULO DÉCIMO -De las Garantías Constitucionales
###### Art. J.01 - Prohibición de la acumulación de cargos

1. Ninguna persona podrá ejercer más de un cargo electivo cuya designación se realice, directa o indirectamente, de forma electiva o política. La asunción de un nuevo cargo produce automáticamente la renuncia irrevocable del anterior.

2. Son cargos de designación electiva aquellos que son elegidos a través de procedimientos electorales, directamente por la ciudadanía o por cargos elegidos de forma electiva.

3. Son cargos de designación política (o de confianza) aquellos que son designados discrecionalmente por los órganos del Estado.

> **Concordancias:** A.01 - Titulo, B.11 - Titulo....
>
> **Equivalencia CE78:** [Art. ??](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a??): ASUNTO, 
>
> * **rmatelec @2024.03.02** ¿El consejo de Justicia encaja en la definición de órgano electivo?
