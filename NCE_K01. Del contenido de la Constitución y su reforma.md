#### CAPITULO UNDÉCIMO - Del contenido de la Constitución y su reforma ####

###### Art. K.01 - Del contenido de la Constitución y de su modificación

1. La Constitución sólo podrá contener preceptos que definan:

    1. Órganos del Estado, así como sus competencias, facultades, forma de elección y ...
    2. Derechos y obligaciones de naturaleza política, o necesarios para el ejercicio de derechos y obligaciones de naturaleza política.
    3. Garantías de la igualdad política, del orden constitucional y los derechos _fundamentales_.
    4. Definiciones de conceptos y principios rectores del ordenamiento jurídico.

2. La Constitución sólo podrá modificarse mediante el _procedimiento de reforma constitucional_ definido en la misma.

> **Concordancias:**
>
> **Equivalencia CE78:** 
>
>

[Anterior]() | [Siguiente](NCE_K02.%20Iniciativa%20de%20reforma.md)
