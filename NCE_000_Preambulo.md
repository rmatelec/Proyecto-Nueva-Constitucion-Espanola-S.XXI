Los hombres y mujeres de España,

1. en nuestro deseo de promover la realización de los más altos ideales humanos y de contribuir a la erradicación definitiva de toda forma de opresión;

2. reconociéndonos mutuamente como iguales e integrantes de una comunidad nacional configurada a lo largo de la historia, con características e intereses propios y compartidos;

3. provistos de la capacidad de decidir, sin injerencias ajenas a nosotros mismos, cómo gobernarnos y el tipo de sociedad en la que queremos vivir;

4. conscientes del peligro de que el Estado utilice sus facultades en provecho propio, o contra quienes debe servir y proteger;

5. y sabedores de que sólo un poder está en disposición de frenar los abusos de otro;

reunimos nuestras voluntades para dotarnos de un marco institucional capaz de salvaguardar de los peores defectos de la naturaleza humana la libertad política de todos.

Conocedores de que toda persona es susceptible de ser corrompida y de que toda organización puede acabar siendo instrumentalizada con fines espurios, no nos pareció sensato tener que confiar en la bondad, rectitud y honradez de funcionarios, políticos y gobernantes.

Por eso, organizamos el Estado sobre una prudente desconfianza hacia cualquier forma de poder y nos dotamos de los mecanismos necesarios para asegurarnos de que todo poder se encuentre siempre sometido a la voluntad colectiva de la ciudadanía, sujeto al constante escrutinio público de sus acciones y expuesto a rendir cuentas por ellas, de modo que sea la Nación la que lo gobierne. No al revés.

Para asegurarnos de que el Estado cumple con los fines que justifican su existencia no sólo establecimos la supremacía de las leyes que la Nación se ha dado y las garantías que protegen a la persona de la dictadura de la mayoría, también limitamos la influencia que el Estado puede ejercer sobre la Nación y dividimos sus funciones en segmentos independientes y enfrentados, separados en origen y situados en un equilibrio estable para que ninguno de ellos pueda dominar a los demás, ni ser dominado por ellos. Desgraciadamente, los controles institucionales no siempre son suficientes.

El peor enemigo de la libertad es una ciudadanía indiferente, temerosa o sumisa que tolera un Estado incontrolado que ejerce su poder sin respetar los límites que le hemos impuesto. Por eso, los derechos e instituciones plasmados en ésta, nuestra Constitución, no pueden darse por garantizados.

Sabemos que los enemigos de nuestra libertad nunca descansan, que la indiferencia y la impunidad fomentan los abusos de poder, que somos tan libres como el menos libre entre nosotros y que el precio de la libertad es la eterna vigilancia.

El único escudo contra la tiranía es nuestra determinación de mantenernos libres y nuestra disposición a realizar los sacrificios que sean necesarios para preservar el modo de vida que hemos elegido. Por eso, creamos también los mecanismos para que la ciudadanía intervenga de forma directa y eficaz en el proceso político siempre que la situación lo requiera.

La fuerza de toda Constitución nace de las personas dispuestas a hacerla cumplir sin importar las consecuencias, que no siempre se encuentran en las instituciones del Estado. Sólo una sociedad civil organizada, informada, activa, vigilante, crítica y responsable; guiada por la razón, el respeto, la decencia y la búsqueda de la verdad; compuesta por personas autónomas y autosuficientes, que entiendan que el Estado existe para servirles y que defiendan activamente sus intereses en el proceso político, está en disposición de gobernar a su Estado.

Por eso, os animamos a que hagáis vuestros estos ideales que inspiran nuestra Constitución y asumáis como propia la misión de protegerla, perfeccionarla y transmitir este legado de libertad y democracia a las generaciones venideras. Que nuestro ejemplo ilumine el camino hacia la emancipación de todos los pueblos de la Tierra.

> **Concordancias:** [Art. A02 - Organización política de la Nación](NCE_A02.%20Organizaci%C3%B3n%20pol%C3%ADtica%20de%20la%20Naci%C3%B3nEstado.md), [Art. A03 - Articulación política del Estado y división de poderes](NCE_A03.%20Articulaci%C3%B3n%20pol%C3%ADtica%20del%20Estado%20y%20divisi%C3%B3n%20de%20poderes.md)
>
> **Equivalencia CE78:** [Preámbulo](https://www.boe.es/buscar/doc.php?id=BOE-A-2014-11064#textoxslt)

[Siguiente](NCE_A01.%20Soberan%C3%ADa%20de%20la%20Naci%C3%B3n%20espa%C3%B1ola.md)
