###### Art. C.30 - Derecho al sufragio

1. Todos los españoles mayores de edad tienen derecho a emitir 

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20): Derecho a la libertad de expresión e información

[Anterior](NCE_C21.%20Derecho%20a%20la%libertad%20de%expresión.md) |


Artículo 23

1. Los ciudadanos tienen el derecho a participar en los asuntos públicos, directamente o por medio de representantes, libremente elegidos en elecciones periódicas por sufragio universal.

2. Asimismo, tienen derecho a acceder en condiciones de igualdad a las funciones y cargos públicos, con los requisitos que señalen las leyes.

