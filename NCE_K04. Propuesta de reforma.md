###### Art. K.03 Propuesta de reforma Constitucional.md ######

1. La _propuesta de reforma constitucional_ :

    1. Sólo puede incluir modificaciones del texto constitucional.
    2. Sólo puede referirse a los contenidos propios de la Constitución, definidos en el artículo [K01](NCE_K01.%20Del%20contenido%20de%20la%20Constitución%20y%20su%20reforma.md).
    3. Debe ir acompañada de los avales necesarios para iniciar el _procedimiento de reforma constitucional_.
    4. Debe identificar a sus promotores e incluir una lista de portavoces, a los que se encarga la defensa de la propuesta.
    5. Puede ir acompañada de instrucciones, mandatos o normas de decisión a aplicar durante su tramitación, que deben haber sido suscritas por todos los avalistas y obligan a los promotores y portavoces.

2. La _propuesta de reforma_ será defendida por hasta 3 portavoces nombrados por los avalistas o, en su defecto, por hasta 3 promotores de la iniciativa.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 166](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a166): Iniciativa de Reforma Constitucional, [Art. 87](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a87): Iniciativa Legislativa (por remisión).
>

[Anterior](NCE_K03.%20Limitaciones%20del%20procedimiento%20de%20reforma.md) | [Siguiente]()
