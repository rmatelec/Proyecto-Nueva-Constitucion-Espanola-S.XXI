###### Art. C.22 - Derecho a la libertad de expresión

1. Toda persona es libre de conocer, expresar y difundir sin injerencias hechos, pensamientos, ideas u opiniones por cualquier medio, incluso cuando pudieran resultar desagradables, ofensivas o aborrecibles.

2. Este derecho puede ejercerse siempre y cuando se haga:
    1. Sin vulnerar los derechos incluidos en el presente Título, en especial los derechos al honor y a la privacidad.
    2.  Sin sugerir, dirigir o alentar la realización de acciones u omisiones que vulneren _derechos fundamentales_ cuando exista riesgo real de que dichas acciones u omisiones pudieran llevarse a cabo de forma inminente.


(1) the speaker must intend to cause violence, (2) he or she must intend that the violence occur immediately, and (3) the violence must be likely to occur immediately.


3. No corresponde a los poderes públicos determinar qué puede o no puede ser comunicado o expresado. Por tanto, no podrán prohibir, limitar o someter a control previo el ejercicio de este derecho.

regular  bajo ningún concepto en función de su contenido o de las posibles reacciones al mismo.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 20](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a20): Derecho a la libertad de expresión e información

[Anterior](NCE_C21.%20Derecho%20a%20la%libertad%20de%expresión.md) |
