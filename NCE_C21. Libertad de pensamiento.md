###### Art. C.21 - Derecho a la libertad de pensamiento

1. Todas las personas tienen derecho a pensar, opinar o creer lo que quieran, sin limitación alguna.
2. Nadie está obligado ni puede ser obligado a:
    1. Adoptar o adherirse a una idea, opinión, creencia o pensamiento en contra de su voluntad.
    2. Permitir el conocimiento, registro o análisis del contenido de sus pensamientos.
    3. Declarar sobre sus pensamientos.
3. Queda igualmente prohibido:
    1. Alterar o modificar los pensamientos ajenos sin la participación consciente y consentida del individuo.
    2. Desvelar o conocer los pensamientos ajenos sin un acto consciente de exteriorización previo.
4. Nadie puede ser molestado, discriminado ni privado de ningún derecho a causa de sus pensamientos.
5. Los pensamientos no eximen a las personas del cumplimiento de sus obligaciones jurídicas, ni pueden limitar a terceras personas el ejercicio de sus derechos.
6. No existen los delitos de opinión o pensamiento. Bajo ninguna circunstancia los pensamientos constituyen prueba o testimonio.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 16](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a16): Derecho a la libertad ideológica y religiosa

[Anterior](NCE_C20.%20Derecho%20a%20la%20vida%20e%20integridad%20de%20la%20persona.md) | [Siguiente](NCE_22.%20Derecho%20a%20la%20libertad%20de%expresión%20e%20información.md)
