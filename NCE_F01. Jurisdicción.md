## TITULO SEXTO - De la Justicia y los medios de fiscalización ##
###### Art. F01 - Jurisdicción ######

1. El ejercicio de la potestad jurisdiccional en todo tipo de procesos, juzgando y haciendo ejecutar lo juzgado, corresponde exclusivamente a los Juzgados y Tribunales determinados por las leyes, según las normas de competencia y procedimiento que las mismas establezcan.


> **Concordancias:**
>
> **Equivalencia CE78: 
>
> **rmatelec @2023.11.01 El Tribunal Constitucional se ha apoyado en la cláusula del Estado de Derecho para rechazar las vías de hecho de los poderes públicos (ATC 525/1987), para exigir la motivación de las sentencias judiciales  (STC 55/1987) o para imponer el carácter obligatorio de su cumplimiento (STC 67/1984).


[Anterior]() | [Siguiente](NCE_F02.%20Órganos%20del%20Poder%20Judicial.md)