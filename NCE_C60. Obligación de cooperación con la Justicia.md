###### Art. C.60 - Obligación de cooperación con la Justicia

1. Es obligado cumplir las sentencias y demás resoluciones firmes de los Jueces y Tribunales, así como prestar la colaboración requerida por éstos en el curso del proceso y en la ejecución de lo resuelto.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 118](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a118): Obligación 
