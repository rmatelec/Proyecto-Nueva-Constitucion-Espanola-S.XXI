## DISPOSICIONES FINALES ##
###### Z.01 - Vigencia de las disposiciones sancionadoras ###### 

1. El requisito formal establecido en el art. C17 relativo a que las definiciones de los hechos punibles y sus sanciones y/o penas deben estar incluidos en el Código Penal o el Código de Derecho Administrativo Sancionador no será aplicable a las disposiciones de carácter punitivo y/o sancionador incluidas en leyes aprobadas con anterioridad a esta Constitución y que no hubieran sido modificadas desde entonces.

> **Concordancias:**
>
> **Equivalencia CE78:** 
>
>

[Anterior]() | [Siguiente](NCE_Z02.%20Disolución%20de%20los%20Órganos%20del%20Estado%20incompatibles%20con%20la%20Constitución.md)

