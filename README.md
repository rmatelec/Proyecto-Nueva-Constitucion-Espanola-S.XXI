Proyecto de una Nueva Constitucion Española para el siglo XXI
=============

Este repositorio alberga un proyecto constitucional elaborado por la sociedad civil de forma colaborativa, descentralizada, transparente y abierta en previsión de la apertura de un eventual proceso constituyente en España.

## Colaborar

Toda colaboración se admite y es bienvenida. Para colaborar eficazmente es recomendable aprender a maquetar los documentos utilizando [Markdown](https://docutils.sourceforge.io/docs/user/rst/quickref.html) y a utilizar [Git](https://github.com/Hispano/Guia-sobre-Git-Github-y-Metodologia-de-Desarrollo-de-Software-usando-Git-y-Github).

Al colaborar declaras ser el autor de los contenidos que aportas y aceptas que se incorporen al proyecto conforme a los términos bajo los que se [licencia](LICENSE.md) el mismo. Si no cumples estos requisitos o no estás de acuerdo, por favor, abstente de colaborar.

El fichero [NCE_XXX.Plantilla.md](NCE_XXX.%20Plantilla.md) contiene una plantilla para subir nuevos artículos. Puedes intentar ubicarlos utilizando la información sobre la estructura de la obra del siguiente apartado. Si aun así tienes dudas, déjalo numerado como XXX pero ponle un nombre único y que describa su contenido.

## Estructura de la obra

Con el objetivo de facilitar la utilización, modificación y expansión del texto legal, en lugar de la tradicional ordenación consecutiva de los artículos se ha optado por otra alfanumérica que refleja la estructura de la norma. Así, cada artículo se identifica con una letra mayúscula que corresponde con la del Título en el que se enmarca, y varios caracteres numéricos que señalan la ordenación del artículo dentro del Título (esta vez sí, de forma consecutiva). Por ejemplo , el apartado primero del artículo segundo del Título III sería el artículo C02.1.

Si existen subdivisiones dentro de cada Título, ("Capítulos"), se deja espacio para añadir, al menos, 10 artículos desde el último del capítulo anterior y se continua la numeración desde el siguiente múltiplo de 10. Por ejemplo, si el Capítulo I acaba en el artículo M43, el Capítulo II comenzará en el artículo M60.

La estructura provisional del nuevo texto constitucional es la siguiente:

> [Preámbulo](NCE_000_Preambulo.md)  
> A - [De España y la nación española](NCE_A01.%20Soberan%C3%ADa%20de%20la%20Naci%C3%B3n%20espa%C3%B1ola.md).  
> B - [De las normas y su interpretación](NCE_B01.%20Supremacía%20de%20la%20Constitución.md).  
> C - [De los derechos y deberes fundamentales](NCE_C01.%20Ámbito%20y%20regulación%20de%20los%20derechos%20fundamentales.md).  
> D - De los Órganos de representación.
> E - [Del poder Ejecutivo](NCE_E01.%20Del%20Gobierno%20del%20Estado.md).  
> F - [De la Justicia y los medios de fiscalización](NCE_F01.%20Jurisdicción.md).  
> G - [De las formas de participación ciudadana](NCE_G01.%20De%20las%20elecciones.md).  
> H - De la Organización Territorial del Estado.  
> I - [Principios rectores del funcionamiento del Estado](NCE_I01.%20Motivaci%C3%B3n%20de%20los%20actos.md).
> J - [Garantías Constitucionales](NCE_J01.%20Prohibici%C3%B3n%20de%20la%20acumulaci%C3%B3n%20de%20cargos.md).
> K - [Reforma de la Constitución](NCE_K01.%20Del%20contenido%20de%20la%20Constitución%20y%20su%20reforma.md).  
> Z - [Disposiciones finales](NCE_Z01.%20Vigencia%20de%20las%20disposiciones%20sancionadoras.md).

## Licencia y Créditos

(c) 2013-2024 - Rubén Maté Lecumberri y colaboradores.    
El contenido de este repositorio se distribuye bajo licencia [Creative Commons Atribución-CompartirIgual 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.es_ES), salvo la imagen del escudo de la III República, que se distribuye bajo licencia [Creative Commons Atribución 4.0](http://creativecommons.org/licenses/by/4.0/deed.es_ES). Si encuentras algo útil, eres libre de utilizarlo respetando las condiciones de la licencia correspondiente.