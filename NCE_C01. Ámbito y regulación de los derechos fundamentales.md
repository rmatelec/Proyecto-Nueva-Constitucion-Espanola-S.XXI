### TITULO TERCERO. - De los derechos y deberes fundamentales

###### Art. C01 - Ámbito y regulación de los derechos fundamentales

1. Todo ser humano goza de los _derechos_ enumerados en el presente Título -denominados _fundamentales_- en cualquier parte del territorio español desde su nacimiento y hasta su muerte, salvo que la propia Constitución disponga otra cosa.
2. Los _derechos fundamentales_ son inviolables, universales, interdependientes, indivisibles y plenamente exigibles sin necesidad de desarrollo normativo previo. La ordenación en que se presentan no determina su jerarquía ni la superioridad de unos sobre otros; aunque prevalencen sobre otros principios constitucionalmente protegidos.
3. En el ejercicio de los _derechos fundamentales_ todos deben garantizar a los demás miembros de la sociedad el disfrute de los mismos derechos, siendo éste el único fundamento constitucional para su limitación mediante _Ley Orgánica_. Cuando no fuera posible la plena satisfacción de todos ellos, se protegeran aquellos cuya vulneración fuera más intensa y lesiva de la forma que menos lesionen el ejercicio de los restantes derechos fundamentales en conflicto.
4. La aprobación de las leyes reguladoras de _derechos fundamentales_ corresponde exclusivamente a la _Asamblea Nacional_. Para su entrada en vigor, cualquier aprobación, modificación o derogación de estas normas deberá ser ratificada mediante referéndum vinculante con el voto favorable de más de la mitad de las personas con derecho a voto.
5. La regulación de _derechos fundamentales_ no podrá dificultar su ejercicio, vaciarlos de contenido, imponer efectos desfavorables para quienes los ejerzan dentro de los límites establecidos en la Constitución, ni prever preveer sanciones que pudieran disuadir de su ejercicio.
6. La interpretación las leyes reguladoras de _derechos fundamentales_ se realizará de conformidad con la Constitución, la Declaración Universal de Derechos Humanos de 1948 y los tratados y acuerdos internacionales sobre las mismas materias ratificados por España.
7. La interpretación de las _normas con fuerza de ley_ que limiten el ejercicio de _derechos fundamentales_ será siempre restrictiva.
8. Las personas jurídicas no disfrutarán en España de estos derechos, sin perjuicio de que la ley les puedan reconocer otros de idéntico contenido pero distinta naturaleza.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 10](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a10): Interpretación conforme a los tratados internacionales
>
