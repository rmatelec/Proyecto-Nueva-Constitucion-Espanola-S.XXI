###### Art. A02 - Organización política de la Nación

1. El Estado español se establece y legitima como instrumento al servicio de la Nación española; configurándose como una República Constitucional.

2. Los altos cargos del Estado son designados para mandatos de duración limitada directamente por la Nación mediante procedimientos electorales democráticos, y responden de sus actos ante la misma.

3. El Estado, y todos sus órganos, tienen encomendado garantizar:
    1. El cumplimiento de esta Constitución, del ordenamiento jurídico de la que es fundamento, y de la voluntad expresada por la mayoría de los españoles dentro del mismo.
    2. El respeto y protección de la dignidad humana, de los _derechos_ denominados _fundamentales_, y de las minorías; procurando que todos disfruten de unas condiciones de vida y de oportunidades adecuadas para alcanzar su pleno desarrollo, y participen de forma equitativa de la prosperidad y riqueza de la nación.
    3. La posibilidad de que todos los ciudadanos participen, de forma efectiva y en condiciones de igualdad, en la vida política, económica, cultural y social; en la toma de decisiones vinculantes que les afecten, y en la administración del país según los principios democráticos; facilitando y fomentando la participación mediante procedimientos inclusivos.
    4. La convivencia pacífica, la unidad nacional, la preservación de la soberanía de la nación Española y la independencia del Estado respecto de poderes ajenos a la voluntad de la Nación española.
    5. La conservación y promoción del patrimonio histórico, natural, cultural, económico y humano de la nación; procurando su aprovechamiento responsable y sostenible.
    6. El desarrollo armonioso de la nación sobre el principio de solidaridad, promoviendo el progreso de cada territorio según sus características y preferencias.
    7. El óptimo funcionamiento de los servicios públicos, las infraestructuras y los mercados, de forma que todos puedan proveerse de los bienes y servicios que requieran con el menor esfuerzo.

4. Corresponde igualmente al Estado español:
    1. Colaborar en el establecimiento de unas relaciones pacíficas y de eficaz cooperación entre todos los pueblos de la Tierra y un orden internacional justo, en el que se respeten los Derechos Humanos y su violación no quede impune.
    2. Proteger a los españoles fuera del territorio nacional.
    3. Velar por los intereses de las generaciones futuras de españoles.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 9.2](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a9),
>

[Anterior](NCE_A01.%20Soberan%C3%ADa%20de%20la%20Naci%C3%B3n%20espa%C3%B1ola.md) | [Siguiente](NCE_A03.%20Articulaci%C3%B3n%20pol%C3%ADtica%20del%20Estado%20y%20divisi%C3%B3n%20de%20poderes.md)
