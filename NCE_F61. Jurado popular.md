###### Art. F61 - Jurado ######

1. Los ciudadanos podrán participar en la Administración de Justicia mediante la institución del Jurado, en la forma y con respecto a aquellos procesos penales que la ley determine, así como en los Tribunales consuetudinarios y tradicionales.


> **Concordancias:**
>
> **Equivalencia CE78: [Art.125](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a125)
>
