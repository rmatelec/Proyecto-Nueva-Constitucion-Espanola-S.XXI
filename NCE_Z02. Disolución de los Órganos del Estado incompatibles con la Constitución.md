###### DT.02 - Disolución de los Órganos del Estado incompatibles con la Constitución ######

1. Se disuelven, con carácter inmediato

    1. El Ministerio de Justicia, quedando atribuidas sus competencias al Consejo de Justicia cuando se forme.
    2. El Consejo General del Poder Judicial, quedando atribuidas sus competencias al Consejo de Justicia cuando se forme.
    3. El Tribunal Constitucional. Se dará traslado de los procedimientos abiertos ante este tribunal a la sala especial del Tribunal Supremo que asuma el control de constitucionalidad último dentro de la jurisdicción.
    4. El defensor del pueblo. Se dará traslado de los procedimientos abiertos ante este órgano a los representantes políticos de los interesados en la Asamblea Nacional.


[Anterior](NCE_Z01.%20Vigencia%20de%20las%20disposiciones%20sancionadoras.md) | [Siguiente]()

