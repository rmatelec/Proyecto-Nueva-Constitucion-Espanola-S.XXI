### TITULO NOVENO -Principios rectores del funcionamiento del Estado
###### Art. I.01 - Motivación de los actos de los Órganos del Estado

1. Los actos de los Órganos de Estado deberán estar siempre suficientemente motivados, debiendo incluirse en ellos:
    a. los preceptos del ordenamiento jurídico que les atribuyen la capacidad para realizar el acto,
    b. los hechos que se han tomado en consideración y los criterios utilizados para resolver en un sentido u otro,
    c. los hechos y criterios que no se han tomado en consideración y el por qué,
    d. el razonamiento jurídico

> **Concordancias:** A.01 - Titulo, B.11 - Titulo....
>
> **Equivalencia CE78:** [Art. ??](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a??): ASUNTO, 
>
