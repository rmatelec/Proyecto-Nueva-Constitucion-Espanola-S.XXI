### TITULO ??? - XXXXXXXXXXXXXXXXXXXXXXXX 

#### CAPITULO  ??? - XXXXXXXXXXXXXXXXXXXXXXXX

##### Art. ?.?? - XXXXXXXXXXXXXXXXXXXXXXXX

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque luctus massa eget magna convallis pharetra. Quisque vehicula luctus felis sed porta. Etiam sollicitudin libero sit amet nibh molestie, eu ultrices arcu pellentesque. Etiam finibus nibh eu magna molestie, quis fermentum magna venenatis. Maecenas imperdiet purus eget vestibulum aliquet. Phasellus molestie viverra consectetur. Duis mauris urna, scelerisque quis lobortis et, consequat id nunc. Aliquam erat volutpat. Sed non scelerisque risus, a egestas arcu.

1. Ut dictum augue elit, a sagittis mi pharetra eget. Proin rutrum convallis metus, eget efficitur quam.
2. Vivamus a felis ullamcorper, bibendum lacus sit amet, interdum turpis. 
3. Nulla metus velit, semper vitae massa sagittis, euismod lacinia mi. 
4. Phasellus non leo gravida, facilisis lorem sit amet, lobortis velit. 

[//]: # (Fin)

[Anterior]() | [Siguiente]()

###### Notas

REFERENCIAS:  [Art.?? CE78](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a??)

CONCORDANCIAS: (dentro del proyecto de Constitución)

PROBLEMAS POTENCIALES: