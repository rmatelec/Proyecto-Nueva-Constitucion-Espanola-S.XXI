###### Art. C04 - Personalidad Jurídica

1. La personalidad jurídica determina la capacidad de los seres humanos para ser titulares de derechos y obligaciones.
2. La personalidad jurídica se adquiere una vez producido el entero desprendimiento del seno materno con vida; y se extingue por la muerte.
3. El concebido se tiene por nacido para todos los efectos que le sean favorables siempre que no se interrumpa la gestación y se terminen produciendo las condiciones que expresa el apartado anterior.

> **Concordancias:**
>
> **Equivalencia CC:** [Art. 29](https://boe.es/buscar/act.php?id=BOE-A-1889-4763#art30) [Art. 30](https://boe.es/buscar/act.php?id=BOE-A-1889-4763#art30) [Art. 32](https://boe.es/buscar/act.php?id=BOE-A-1889-4763#art32)
